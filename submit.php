<?php
    if(isset($_POST['btn_submit']))
    {
        // Loop through each $_POST value   
        foreach($_POST as $key => $field)
        {
            // echo $key . "<br>";
            // Checking if it is name1, name2 or name3.
            if($key == 'name1' || $key == 'name2' || $key == 'name3')
            {
                // return error if name length will be less then 3 or it is not a character
                if(strlen($field) < 3 || !preg_match('/^[a-zA-Z\s\,]+$/', $field))
                {
                    echo '<h3>ERROR</h3><br><a href="submit.php">Please try Again</a>';
                    exit;
                }
            }  // Checking if it is healthid1, healthid2 or healthid3.
            else if($key == 'healthid1' || $key == 'healthid2' || $key == 'healthid3')
            {
                // return error if healthid length will be less then 5 or it is not a number
                if(strlen($field) < 5 || !is_numeric($field))
                {
                    echo '<h3>ERROR</h3><br><a href="submit.php">Please try Again</a>';
                    exit;
                }
            }
        }

        // Return Success msg if no error found in name and healthid
        echo '<h3>SUCCESS</h3><br><a href="submit.php">Click here</a> to Submit again';
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="jumbotron mt-5">
                    <form action="" method="POST">
                        <?php 
                            // Creating field using for loop because all the fields will be same for name & healthid respectively except field number (name1, name2, name3 & healthid1, healthid2, healthid3) .
                            
                            for($i = 1; $i <= 3; $i++)
                            {
                                ?>
                                    <div class="form-group">
                                        <label for="name<?php echo $i; ?>">Name <?php echo $i; ?>:</label>
                                        <input type="text" class="form-control" placeholder="Enter Name <?php echo $i; ?>" id="name<?php echo $i; ?>" name="name<?php echo $i; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="healthid<?php echo $i; ?>">Healthid <?php echo $i; ?>:</label>
                                        <input type="text" class="form-control" placeholder="Enter Healthid <?php echo $i; ?>" id="healthid<?php echo $i; ?>" name="healthid<?php echo $i; ?>">
                                    </div>
                                <?php
                            }
                        ?>
                        <button type="submit" class="btn btn-primary" name="btn_submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>