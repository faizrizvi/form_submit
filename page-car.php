<?php
/*
    Template Name: Page car
*/
$args = array(
    'post_type'		 =>	'automobiles',  // get from only automobiles post type
    'post_status'	 =>	'publish',  // get only published post
    'posts_per_page' => -1,     // for selecting all post
    
);
$automobiles = new WP_Query($args); // Passing the argument to WordPress Query class.

// Checking if there is any post or not
if($automobiles->have_posts())
{
    // Displaying title in unordered list
    echo "<ul>";
    while($automobiles->have_posts())
    {
        $automobiles->the_post();
        echo "<li>" . get_the_title() . "</li>";    // get_the_title() will return title of the post
    }
    wp_reset_postdata();    // Restoring the $post global
    echo "</ul>";
}
else
{
    echo "<p>No post found.</p>";
}
?>